<?php

/* 

$_myvar      //es una variable inicia con $ y contiene underscore
$_7var       //es una variable ya que aunque tiene numero, esta variable inicia con $ y contiene underscore
myvar        // no es una variable porque no inicia con $
$myvar       // es una variable incia con $ y una letra
$var7        //es una variable incia con $ y una letra
$_element1   //es una variable ya que aunque tenga numero, incia con $ y una letra
$house*5     //no lo es ya que contiene un signo aritmetico y no esta permitido aunque inicie con $

*/

//ejercicio 2 iniciso a
$a = "ManejadorSQL";
$b = 'MySql';
$c = &$a;

echo "Soy a y contengo: $a <br>";
echo "Soy b y contengo: $b <br>";
echo "Soy c y contengo: $a <br>";

//ejercicio 2 incisos b y c

$a = "PHP server";
$b = &$a;

echo "Soy el nuevo a y contengo: $a <br>";
echo "Soy el nuevo b y contengo: $b <br>";
echo "Soy otra vez c y contengo: $c <br>";

//ejercicio 2 inciso d
print "<br> Al volver a imprimir, los nuevos a y b apuntan al 
contenido del nuevo a,esto sucede porque las primeras tres variables
ya han sido impresas a la pantalla, por lo que el compilador vuelve a leer las nuevas
asiganciones haciendo que ignore a las variables anteriores <br>";
unset($a, $b, $c);

//ejercicio 3
echo "<br> ---------------Ejercicio 3----------------------------------------- <br>";
$a = "PHP5";
echo "soy a: $a <br>";
$z[] = &$a;
echo('<pre>');
print_r($z);
echo('</pre>');
$b = "5a version de PHP";
settype($b,"integer"); //evitar etiqueta warning al imprimir c, al poner settype b solo queda con el valor de 5 ya que es un tipo integer
echo "soy b: $b <br>";  
$c = $b*10;
echo "soy c: $c <br>";
$a .= $b;
echo "soy a: $a <br>";
$b *= $c;
echo "soy b: $b <br>";
$z[0] = "MySQL";
echo('<pre>');
print_r($z);
echo('</pre>');

//ejercicio 4 
echo "<br> --------------Ejercicio 4-------------------------------------------- <br>";

function test(){

	$a = "PHP5";
	echo 'Soy a, en el ambito gobal:' . $GLOBALS["a"] . "<br>";

	$z[] = &$a;
	echo('Soy el arreglo z apuntando hacia "a" globalmente <pre>');
	print_r($GLOBALS['z']);
	echo('</pre>');

	$b = "5a version de PHP";
	settype($b,"integer");
	echo 'Soy b, en el ambito gobal:' . $GLOBALS["b"] . "<br>";

	$c = $b*10;
	echo 'Soy c, en el ambito gobal:' . $GLOBALS["c"] . "<br>";

	$a .= $b;
	echo 'Soy a, en el ambito gobal:' . $GLOBALS["a"] . "<br>";

	$b *= $c;
	echo 'Soy b, en el ambito gobal:' . $GLOBALS["b"] . "<br>";

	$z[0] = "MySQL";
	echo('Soy el areflo z <pre>');
	print_r($GLOBALS['z']);
	echo('</pre>');

}

test();
unset($a, $b, $c, $z);

//ejercicio 5
echo "<br> --------------Ejercicio 5-------------------------------------------- <br>";
$a = "7 personas";
$b = (integer) $a;
$a = "9E3";
$c = (double) $a;
echo "Soy a: $a <br>";
echo "Soy b: $b <br>";
echo "Soy c: $c <br>";
unset($a, $b, $c);

//ejercicio 6
echo "<br> --------------Ejercicio 6-------------------------------------------- <br>";
$a = "0";
$b = "TRUE";
$c = FALSE;
$d = ($a OR $b);
$e = ($a AND $c);
$f = ($a XOR $b);

echo "soy a" . var_dump((bool) $a) . "<br>";
echo "soy b" . var_dump((bool) $b) . "<br>";
echo "soy c" . var_dump((bool) $c) . "<br>";
echo "soy d" . var_dump((bool) $d) . "<br>";
echo "soy e" . var_dump((bool) $e) . "<br>";
echo "soy f" . var_dump((bool) $f) . "<br>";

settype($c, "int");
echo "Imprimiendo el booleano c $c <br>";
settype($e, "int");
echo "Imprimiendo el booleano e $e <br>";
unset($a, $b, $c, $d, $e, $f);

echo "<br> --------------Ejercicio 7-------------------------------------------- <br>";

echo "Version de Apache y PHP ......" . $_SERVER['SERVER_SOFTWARE'] . "<br>";
echo "Nombre del Sistema Opertivo (servidor) ...... " . $_SERVER['SERVER_NAME'] . "<br>";
echo "Idioma del navegador ......." . $_SERVER['HTTP_ACCEPT_LANGUAGE'] . "<br>";

?>